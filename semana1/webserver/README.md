# Apache en docker

En este ejemplo se leventa un contenedor con apache y una pagina de ejemplo. Se utiliza como base la [imagen oficial de apache](https://hub.docker.com/_/httpd/). En las carpeta web estan los archivos de la pagina de ejemplo.

## Como construir la imagen

En el directorio actual ejecutar

```bash
docker build -t apache-web .
```

## Como correr el contenedor

Para correr el servidor apache en el puerto 8080 del host ejecutar:

```bash
docker run -ti -p 8080:80 --name httpd-container apache-web
```

Ahora se puede acceder a la web a traves de [http://localhost:8080](http://localhost:8080)

> **Tip!** si salis de la consola del contendor el contenedor se termina y te quedas sin webserver 😨.
> Para salir del contendor sin terminarlo(*detached*) usa `Ctrl+P, Ctrl+Q`.
> Para iniciar el contenedor en modo *detached* agrega la flag `-d` y luego podes ingresar a la bash del contendor con el comando `docker exec -ti [ID_CONTAINER] bash`
