# Diferencias entre CMD y ENTRYPOINT

- CMD puede sobreescribirse, ENTRYPOINT es el primer comando que se ejecuata el contenedor y es propio de cada imagen. CMD son los argumentos de ENTRYPIONT
- Es obligatorio que una imagen tenga o CMD o ENTRYPOINT. Si no se define uno, se usara el entrypoint de la imagen que esta en FROM.
- A diferencia de RUN solo puede haber un CMD en la descripcion de la imagen, si hay mas de un CMD solo el ultimo sera valido.
- Si el docker se quiere usar como ejecutable, se debe definir un ENTRYPOINT.