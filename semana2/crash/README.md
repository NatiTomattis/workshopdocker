1. Estando en la carpeta crash, ejecuten $docker build -t crashimage .
2. Para correr la imagen, ejecuten $docker run -d --name willcrash crashimage
3. Fijense que el contenedor está corriendo, pero después de unos segundos se termina con error!
4. Pueden usar --restart on failure/restart always u otras opciones para lograr que su contenedor siempre está corriendo. Prueben con $docker run -d --name willnotcrash --restart on-failure:5 crashimage
¿Que creen que va a pasar?
En el caso de restart on failure, el contenedor solamente es reiniciado cuando falla, no cuando ejecuta exitosamente. Cambien el archivo crash.sh para que el contendor termine de forma exitosa.

