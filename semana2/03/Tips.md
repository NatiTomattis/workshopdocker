# Diferencias entre ADD y COPY

- ADD permite descargar carpetas y archivos desde URL's
- ADD permite descomprimir archivos gzip, bzip2 o xz
- COPY permite recibir archivos de otro contenedor cuando el Dockerfile describe un *multi-stage build* (lo vemos mas adelante) con la flag `--from`
- COPY y ADD solo pueden copiar archivos del host que estan en el *build context*
- Ambos permiten cambiar los permisos de usuario al copiar la carpetas
