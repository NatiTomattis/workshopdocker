# Ejercicios dia 3

Lista de intrucciones mas utiles:

| COMANDO    | USO                                                                                                  |
|------------|------------------------------------------------------------------------------------------------------|
| build      | indica el path a un Dockerfile, para construir la imagen que se va a utilizar                        |
| image      | la imagen que va a usar el servicio (no se puede usar junto con build)                               |
| command    | sobrescribe el comando por defecto del contenedor                                                    |
| depends-on | espera a que otro servicio inicie antes de iniciar                                                   |
| networks   | conecta al contenedor a redes administradas por docker                                               |
| ports      | expone puertos y los mapea con puertos del host                                                      |
| volumes    | conecta volumenes al contenedor, pueden ser volumenes nombrados o los creados por defecto por docker |

Para mas info, leer [Docker Compose Refernce](https://docs.docker.com/compose/compose-file/compose-file-v2/)

Lista de comandos administrativos:

```bash
Sintaxis
docker-compose [CMD] [SRV]
```

| COMANDO    | USO                                                                                                  |
|------------|------------------------------------------------------------------------------------------------------|
| build      | Si existen instrucciones build en el archivo yml, contruye las imagenes                              |
| up         | Inicia los servicios, si las imagenes no estan construidas, las construye                            |
| down       | Elimina los contenedores y las redes pero no los volumenes, para eso agregar flag --volumes          |
| ps         | Musetra el estado de los servicos desfinidos en el archivo docker-compose                            |
| networks   | conecta al contenedor a redes administradas por docker                                               |
| ports      | expone puertos y los mapea con puertos del host                                                      |
| volumes    | conecta volumenes al contenedor, pueden ser volumenes nombrados o los creados por defecto por docker |

*Si tu archivo docker compose no se llama docker-compose.yml, tesnes que agregar la flag -f seguida del nombre del archivo*

## Redes en Docker

```bash
docker network create --driver=bridge --subnet=172.28.0.0/16 --ip-range=172.28.5.0/24 --gateway=172.28.5.254 mi_red
```

## Ejercicio 1

Correr el contenedor del dia 1 con docker compose


## Ejercicio 2

Probar redes de docker con docker-counter

## Ejercicio 3

Agregar HA proxy al ejercicio anterior, completar el docker compose.
Darles el archivo de configuracion de HA proxy con el docker compose para levantar haproxy solo.
Ellos tendrian que juntar los dos ejercicios anteriores con este.

## Ejercicio 4

Aca iria el quagga